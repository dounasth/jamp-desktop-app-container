package com.company;

import ch.vorburger.exec.ManagedProcessException;
import ch.vorburger.mariadb4j.DB;
import ch.vorburger.mariadb4j.DBConfigurationBuilder;

/**
 * Created by ntounas on 15/3/2019.
 */
public class MariaDB {

    public MariaDB(String dbDataDir, int port) throws ManagedProcessException {
        //  MariaDB
        //  DB db = DB.newEmbeddedDB(3306);
        DBConfigurationBuilder configBuilder = DBConfigurationBuilder.newBuilder();
        configBuilder.setPort(port); // OR, default: setPort(0); => autom. detect free port
        configBuilder.setDataDir(dbDataDir); // example "/home/theapp/db"
        DB db = DB.newEmbeddedDB(configBuilder.build());
        db.start();
        System.out.println(db.getConfiguration().getPort());
    }
}
