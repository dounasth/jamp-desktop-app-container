package com.company;

import java.io.IOException;

/**
 * Created by ntounas on 15/3/2019.
 */
public class PHPBuiltIn {
    public PHPBuiltIn(String phpPath, String wwwDir, String host) throws IOException {
        String php = phpPath + "php.exe";
        //  PHP server
        Runtime rt = Runtime.getRuntime();
        String command = php + " -S "+host+" -t \""+wwwDir+"\"";
        Process pr = rt.exec(command);
        System.out.println(command);
        System.out.println(pr);
    }
}
