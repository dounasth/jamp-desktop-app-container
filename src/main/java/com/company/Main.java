package com.company;

import ch.vorburger.exec.ManagedProcessException;
import ch.vorburger.mariadb4j.DB;
import ch.vorburger.mariadb4j.DBConfigurationBuilder;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Scene;
import javafx.scene.control.MenuButton;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import org.cef.CefFrame;
import org.cef.OS;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws ManagedProcessException, IOException {
        Config config = new Config();
        String thisDir = System.getProperty("user.dir") + File.separator + "stash" + File.separator;
        String php = thisDir + "php" + File.separator + config.get("php") + File.separator;
        String dbDataDir = thisDir + "db_data";
        String wwwDir = thisDir + "www" + File.separator + "public"; // eg "C:\\wamp64\\www";
        String host = config.get("host");
        Runtime rt = Runtime.getRuntime();

        System.out.println("thisDir = " + thisDir);
        System.out.println("php = " + php);
        System.out.println("dbDataDir = " + dbDataDir);
        System.out.println("wwwDir = " + wwwDir);
        System.out.println("host = " + host);

        new TrayMenu();
        new MariaDB(dbDataDir, Integer.parseInt(config.get("mariadb.port")));
        new PHPBuiltIn(php, wwwDir, host);

//        JOptionPane.showMessageDialog(null, "App started, will open your browser");
//        Process prb = rt.exec("cmd /c start http://"+host);

        CefFrame frame = new CefFrame("http://"+host, OS.isLinux(), false, args);
    }

}
