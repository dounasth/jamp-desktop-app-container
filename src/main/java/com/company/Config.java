package com.company;

import java.io.*;
import java.util.Properties;

/**
 * Created by ntounas on 15/3/2019.
 */
public class Config {

    Properties prop = new Properties();
    String filename = "config.properties";

    public Config() {
        load();
    }

    public String get(String key) {
        return prop.getProperty(key);
    }

/*    public void update() {
        OutputStream output = null;
        try {
            output = new FileOutputStream(filename);
            // set the properties value
            prop.setProperty("database", "localhost");
            prop.setProperty("dbuser", "mkyong");
            prop.setProperty("dbpassword", "password");
            // save properties to project root folder
            prop.store(output, null);
        } catch (IOException io) {
            io.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }*/

    public void load() {
        InputStream input = null;
        try {
            input = new FileInputStream(filename);
            // load a properties file
            prop.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
